Feature: Login for leaftaps

Background:
Given Open Chrome browser
And Maximize the browser
And set the timeout
And Enter the url
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
When click on login button
Then verify login is success
And click on CRM/SFA 
And click leads
And click Create Lead


Scenario: positive flow

And Enter the company name as Amazon
And Enter the first name as Vidhya	
And Enter the last name as S
When click on create lead button
Then verify create lead is success

Scenario: negative flow
And Enter the first name as Vidhya	
And Enter the last name as S
When click on create lead button
Then verify create lead is fail



