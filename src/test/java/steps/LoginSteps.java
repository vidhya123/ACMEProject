package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	ChromeDriver driver;
	
	
	@Before
	public void beforeScenario(Scenario Sc) {
		System.out.println(Sc.getName());
		System.out.println(Sc.getId());
		}
	
	@After
	public void afterScenario(Scenario Sc) {
	System.out.println(Sc.getStatus());
	System.out.println(Sc.isFailed());
	}
	
	
	
	@Given("Open Chrome browser")
	public void open_Chrome_browser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		// Write code here that turns the phrase above into concrete actions
		 
	}
	
		


	@Given("Maximize the browser")
	public void maximize_the_browser() {
		driver.manage().window().maximize();
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("set the timeout")
	public void set_the_timeout() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	 // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("Enter the url")
	public void enter_the_url() {
		driver.get("http://leaftaps.com/opentaps");
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("Enter the username as (.*)")
	public void enter_the_username(String uname) {
		driver.findElementById("username").sendKeys(uname);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Given("Enter the password as (.*)")
	public void enter_the_password(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("click on login button")
	public void click_on_login_button() {
		driver.findElementByClassName("decorativeSubmit").click();
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("verify login is success")
	public void verify_login_is_success() {
		System.out.println("Login is succesfully completed");
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("click on CRM\\/SFA")
	public void click_on_CRM_SFA() {
		driver.findElementByLinkText("CRM/SFA").click();
		 // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("click leads")
	public void click_leads() {
		driver.findElementByLinkText("Leads").click();
	// Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("click Create Lead")
	public void click_Create_Lead() {
		driver.findElementByLinkText("Create Lead").click();
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("Enter the company name as (.*)")
	public void enter_the_company_name_as_Amazon(String Cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("Enter the first name as (.*)")
	public void enter_the_first_name_as_Vidhya(String Fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("Enter the last name as (.*)")
	public void enter_the_last_name_as_S(String Lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("click on create lead button")
	public void click_on_create_lead_button() {
		driver.findElementByName("submitButton").click();
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@Then("verify create lead is success")
	public void verify_create_lead_is_success() {
		System.out.println("Create lead page is success");
	    // Write code here that turns the phrase above into concrete actions
	    
}
	
	@Then("verify create lead is fail")
	public void verify_create_lead_is_fail() {
		System.out.println("Verify create lead is fail");
	    // Write code here that turns the phrase above into concrete actions
	}
}
