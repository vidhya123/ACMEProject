package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMEVendorSearchpage extends ProjectMethods{

	public ACMEVendorSearchpage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	
	@FindBy(how = How.ID,using="vendorTaxID") WebElement elevendorID;
	@FindBy(how = How.ID,using="buttonSearch") WebElement search;
		
	public ACMEVendorSearchpage enterID() {
		clearAndType(elevendorID, "RO874232");
		return this; 
	}
	public ACMEVeSearchResPage clicksearch() {
        click(search);
        return new ACMEVeSearchResPage();
        
	}
	
	     
	}














