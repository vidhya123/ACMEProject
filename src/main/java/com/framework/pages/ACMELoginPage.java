package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMELoginPage extends ProjectMethods{

	public ACMELoginPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="email") WebElement eleemail;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleclick;
	//@FindBy(how= How.XPATH,using="//button[text()=' Vendors']") WebElement vendor;
	//@FindBy(how= How.XPATH,using="//a[text()='Search for Vendor']") WebElement Click;
	//@FindBy(how = How.ID,using="vendorTaxID") WebElement elevendorID;
	//@FindBy(how = How.ID,using="buttonSearch") WebElement search;
		
	public ACMELoginPage enteremail() {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleemail, "vidhya.ece81@gmail.com");
		return this; 
	}
	public ACMELoginPage enterPassword() {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "krishnakrishna");
		return this;
	}
	public ACMEDashBoard clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleclick);
	     return new ACMEDashBoard();
	     
	}
}













