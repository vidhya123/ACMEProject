package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ACMEDashBoard extends ProjectMethods{

	public ACMEDashBoard() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	
	@FindBy(how= How.XPATH,using="//button[text()=' Vendors']") WebElement elevendor;
	@FindBy(how= How.XPATH,using="//a[text()='Search for Vendor']") WebElement Click;
	//@FindBy(how = How.ID,using="vendorTaxID") WebElement elevendorID;
	//@FindBy(how = How.ID,using="buttonSearch") WebElement search;
		
	public ACMEDashBoard performvendor() {
		moveToElementAct(elevendor);
		return this; 
	}
	public ACMEVendorSearchpage click() {
		moveToElementActClick(Click);
		return new ACMEVendorSearchpage();
	}

	     
	}














