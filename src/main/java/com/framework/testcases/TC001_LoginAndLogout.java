package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
    public void setData() {
		testCaseName = "TC001_LoginAndLogout";
		testDescription = "login to leaftaps";
		testNodes = "Leads";
		author = "Vidya";
		category = "smoke";
		dataSheetName = "TC001";
		
	}

	@Test(dataProvider="fetchData")
	public void login(String uname, String pwd) {

		new LoginPage()
		.enterUsername(uname) 
		.enterPassword(pwd)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickCreateLeadButton();

		/*LoginPage p = new LoginPage();
		p.enterUsername();
		p.enterPassword();
		p.clickLogin();*/
	}

}
