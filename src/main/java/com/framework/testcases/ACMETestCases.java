package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.ACMELoginPage;
import com.framework.pages.LoginPage;

public class ACMETestCases extends ProjectMethods{
	
	@BeforeTest
    public void setData() {
		testCaseName = "ACMETestCases";
		testDescription = "login to ACME";
		testNodes = "ACME";
		author = "Vidya";
		category = "smoke";
	

}
	
	@Test
	public void login() {

		new ACMELoginPage()
		.enteremail()
		.enterPassword()
		.clickLogin()
		.performvendor()
		.click()
		.enterID()
		.clicksearch()
		.displaytext();
		
		
		
		
	}
	
	}
